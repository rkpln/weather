//
//  City.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation


// MARK: – Keys
private struct Keys {
    static let Name = "name"
    static let NormalizedName = "name_normalized"
}


// MARK: – City
public final class City: NSManagedObject {

    @NSManaged var id: NSNumber!
    @NSManaged var weatherReport: WeatherReport?
    
    var name: String {
        get {
            willAccessValueForKey(Keys.Name)
            let aName = primitiveName
            didAccessValueForKey(Keys.Name)
            return aName
        }
        set {
            willAccessValueForKey(Keys.Name)
            primitiveName = newValue
            updateNormalizedName(newValue)
            didAccessValueForKey(Keys.Name)
        }
    }
    
    var countryName: String {
        return NSLocale.currentLocale().displayNameForKey(NSLocaleCountryCode, value: countryISO) ?? countryISO
    }

    // MARK: – Private
    @NSManaged private var primitiveName: String
    @NSManaged private var countryISO: String

    private func updateNormalizedName(value: String) {
        setValue(value.normalizedForSearch, forKey: Keys.NormalizedName)
    }
}


// MARK: – ManagedObjectType
extension City: ManagedObjectType {

    public static var entityName: String {
        return "City"
    }
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: Keys.Name, ascending: true)]
    }
}

// MARK: – City Ext: Search
extension City {
    
    static func searchPredicate(searchTerm: String) -> NSPredicate {
        return NSPredicate(format: "%K BEGINSWITH[n] %@", Keys.NormalizedName, searchTerm.normalizedForSearch)
    }
}

// MARK: – DeserializableManagedObjectType
extension City: DeserializableManagedObjectType {
    private struct JSONKeys {
        static let Country: String = "country"
        static let Identifier: String = "_id"
        static let Name: String = "name"
    }

    public static func deserialize(fromJSONDictionary: [String : AnyObject], andInsertInContext context: NSManagedObjectContext) -> City? {
        guard
            let id = fromJSONDictionary[JSONKeys.Identifier] as? Int,
            let name = fromJSONDictionary[JSONKeys.Name] as? String,
            let country = fromJSONDictionary[JSONKeys.Country] as? String
        else { return nil }
        
        let city: City = context.insertObject()
        city.name = name
        city.id = id
        city.countryISO = country
        return city
    }
}
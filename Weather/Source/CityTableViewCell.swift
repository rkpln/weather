//
//  CityTableViewCell.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/24/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit


// MARK: – Text Attributes
private struct CityTableViewCellTextAttributes {

    static let cityColor = UIColor.blackColor()
    static let cityFont = UIFont(name: "Georgia", size: 24)!

    static let countryColor = UIColor.grayColor()
    static let countryFont = UIFont(name: "Georgia", size: 16)!
}


final class CityTableViewCell: UITableViewCell {

    @IBOutlet var label: UILabel!
}


// MARK: – ConfigurableCell
extension CityTableViewCell: ConfigurableCell {
        
    func configureForObject(city: City, inEnvironment _: AnyObject? = nil) {
        
        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: city.name + " ", attributes: [
            NSForegroundColorAttributeName : CityTableViewCellTextAttributes.cityColor,
            NSFontAttributeName : CityTableViewCellTextAttributes.cityFont
        ])
        
        attrString.appendAttributedString(NSMutableAttributedString(string: city.countryName, attributes: [
            NSForegroundColorAttributeName : CityTableViewCellTextAttributes.countryColor,
            NSFontAttributeName : CityTableViewCellTextAttributes.countryFont
        ]))
        
        label.attributedText = attrString
    }
}

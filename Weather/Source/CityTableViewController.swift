//
//  CityTableViewController.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/24/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit
import CoreData


// MARK: – Keys
private struct CityTableViewControllerKeys {
    static let EmptyString = ""
    static let CellIdentifier = "CityCell"
}


final class CityTableViewController: UITableViewController, ManagedObjectContextSettable {

    var managedObjectContext: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        definesPresentationContext = true
    }
    
    // MARK: – Private
    private var searchController: UISearchController!
    private typealias CityDataProvider = FetchedResultsDataProvider<CityTableViewController>
    private var dataProvider: CityDataProvider!
    private var dataSource: TableViewDataSource<CityTableViewController, CityDataProvider, CityTableViewCell>!
    
    private func setUpTableView() {
        setUpSearchController()
        setUpDataSource()
        tableView.tableHeaderView = searchController.searchBar
        tableView.tableFooterView = UIView()
    }
    
    private func setUpSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
    }
    
    private func setUpDataSource() {
        let request = NSFetchRequest(entityName: City.entityName)
        request.predicate = City.searchPredicate(searchString)
        request.sortDescriptors = City.defaultSortDescriptors
        request.fetchBatchSize = 25
        request.returnsObjectsAsFaults = false
        let fetchRequestController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        dataProvider = FetchedResultsDataProvider(fetchedResultsController: fetchRequestController, delegate: self)
        dataSource = TableViewDataSource(tableView: tableView, dataProvider: dataProvider, delegate: self)
    }
    
    private var searchString: String {
       return searchController.strippedString ?? CityTableViewControllerKeys.EmptyString
    }
}


// MARK: –  UITableViewDelegate
extension CityTableViewController {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        searchController.searchBar.resignFirstResponder()
        let city: City = dataProvider.objectAtIndexPath(indexPath)
        if city.weatherReport == nil {
            WeatherReport.insert(inMOC: managedObjectContext, city: city)
        }
        performSegue(SegueIdentifier.CloseSearch)
    }
}

// MARK: – DataSourceDelegate
extension CityTableViewController: DataSourceDelegate {
    
    func cellIdentifierForObject(object: City) -> String {
        return CityTableViewControllerKeys.CellIdentifier
    }
    
    func environmentForObject(object: City) -> AnyObject? {
        return nil
    }
}


// MARK: – DataProviderDelegate
extension CityTableViewController: DataProviderDelegate {
    
    func dataProviderDidUpdate(updates: [DataProviderUpdate<City>]?) {
        dataSource.processUpdates(updates)
    }
    
    func dataProviderShouldRequestDataUpdate() -> Bool {
        return searchController.hasValidText
    }
}


// MARK: – UISearchResultsUpdating
extension CityTableViewController: UISearchResultsUpdating {
 
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        dataProvider.reconfigureFetchRequest { (request) in
            request.predicate = City.searchPredicate(searchString)
        }
    }
}


// MARK: – UISearchBarDelegate
extension CityTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: – UISearchControllerDelegate
extension CityTableViewController: UISearchControllerDelegate {
}


// MARK: – SegueHandlerType
extension CityTableViewController: SegueHandlerType {
    
    enum SegueIdentifier: String {
        case CloseSearch = "exitSearch"
    }
}

// MARK: – UISearchController
private extension UISearchController {
    
    var hasValidText: Bool {
        return strippedString != nil
    }
    
    var strippedString: String? {
        guard let text = searchBar.text else { return nil }
        let strippedString = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        guard strippedString.isEmpty == false else { return nil }
        return strippedString
    }
}
//
//  ConcurrentOperation.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/30/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation


enum State: String {
    case Ready = "isReady"
    case Executing = "isExecuting"
    case Finished = "isFinished"
}


class ConcurrentOperation: NSOperation {

    var state: State {
        willSet {
            willChangeValueForKey(state.rawValue)
            willChangeValueForKey(newValue.rawValue)
        }
        didSet {
            didChangeValueForKey(oldValue.rawValue)
            didChangeValueForKey(state.rawValue)
        }
    }
    
    override var ready: Bool {
        return state == .Ready && super.ready
    }
    
    override var executing: Bool {
        return state == .Executing
    }
    
    override var finished: Bool {
        return state == .Finished
    }
    
    override var asynchronous: Bool {
        return true
    }
    
    override init() {
        state = State.Ready
        super.init()
    }
}

//
//  ConditionUnits.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/25/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation


// MARK: – TemperatureUnit
enum TemperatureUnit: Int {
    case Kelvin = 0
    case Celsius = 1
    case Fahrenheit = 2
    
    var displayString: String {
        switch self {
            case .Kelvin: return "\u{00B0}K"
            case .Celsius: return "\u{00B0}C"
            case .Fahrenheit: return "\u{00B0}F"
        }
    }
}


// MARK: – TemperatureType
protocol TemperatureType {
    func value(inUnit unit: TemperatureUnit) -> Double
}


// MARK: – KelvinTemperature
struct KelvinTemperature {

    private let value: NSNumber!
    init(_ rawValue: NSNumber) {
        value = rawValue
    }
}


// MARK: – KelvinTemperature Ext: TemperatureType
extension KelvinTemperature: TemperatureType {
    
    func value(inUnit unit: TemperatureUnit) -> Double {
        switch unit {
        case .Kelvin :
            return value.doubleValue
        case .Celsius:
            return (value.doubleValue - 273.15)
        case .Fahrenheit:
            return (value.doubleValue - 273.15) * 1.80000 + 32
        }
    }
}

extension KelvinTemperature: Equatable {}

func ==(lhs: KelvinTemperature, rhs: KelvinTemperature) -> Bool {
    return lhs.value(inUnit: .Kelvin) == rhs.value(inUnit: .Kelvin)
}
//
//  Conditions.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/23/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData


// MARK: – Keys
private struct Keys {
    static let Date = "date"
}


// MARK: – Conditions
class Conditions: NSManagedObject {

    @NSManaged var date: NSDate?
    @NSManaged var humidity: NSNumber?
    @NSManaged var pressure: NSNumber?
    @NSManaged var conditionsOf: Forecast?
    
    var temperatureUnit: KelvinTemperature {
        return KelvinTemperature(temperature)
    }

    // MARK: – Private
    @NSManaged private var temperature: NSNumber!
}


// MARK: – ManagedObjectType
extension Conditions: ManagedObjectType {
    static var entityName: String {
        return "Conditions"
    }
    
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: Keys.Date, ascending: false)]
    }
}

//
//  ConfigurableCell.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/17/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation

protocol ConfigurableCell: class {
    associatedtype DataSource
    associatedtype Environment
    func configureForObject(object: DataSource, inEnvironment _: Environment)
}

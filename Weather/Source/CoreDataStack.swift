//
//  CoreDataStack.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData


// MARK: – Keys
private struct Keys {
    static let JSONFileName = "City"
    static let JSONFileExtenstion = "json"
    static let DatabaseFileName = "Weather"
    static let DatabaseFileExtenstion = "sqlite"
    static let DatabaseMovedFlag = "didMoveDatabase"
}


// MARK: – URLS
private let targetBundle = NSBundle(forClass: City.self)
private let targetStoreURL = NSURL.documentsDirectoryURL.URLByAppendingPathComponent(Keys.DatabaseFileName + "." + Keys.DatabaseFileExtenstion)
private let prepopulatedStoreURL = targetBundle.URLForResource(Keys.DatabaseFileName, withExtension: Keys.DatabaseFileExtenstion)


// MARK: – Main Context
private func setUpMainContext() -> NSManagedObjectContext {
    guard let model = NSManagedObjectModel.mergedModelFromBundles([targetBundle]) else { fatalError("Managed Object Model is not found") }
    
    let storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
    try! storeCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: targetStoreURL, options: nil)
    let context = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
    context.persistentStoreCoordinator = storeCoordinator
    context.undoManager = nil
    return context
}


// MARK: – NSManagedObjectContext Ext
extension NSManagedObjectContext {
    
    private func saveOrRollback() -> Bool {
        do {
            try save()
            return true
        } catch {
            rollback()
            return false
        }
    }
    
    func performSaveOrRollback() {
        performBlock {
            self.saveOrRollback()
        }
    }
    
    func performChanges(block: () -> ()) {
        performBlock {
            block()
            self.saveOrRollback()
        }
    }
}


// MARK: – Prepopulate database, only for DEV phase. Application should be always shipped with prepopulated database. Otherwise it's gonna be ineffective. Might be a good idea to move it in separate OS X util with same database, but no point in doing it for test assignment.

private extension NSUserDefaults {
    
    static var didMovePrepopulatedDatabase: Bool {
        get {
            return standardUserDefaults().boolForKey(Keys.DatabaseMovedFlag)
        }
        set {
            standardUserDefaults().setBool(newValue, forKey: Keys.DatabaseMovedFlag)
        }
    }
}


public struct PrepopulatedCoreDataStack {

    static func prepareAndReturnMainContext() -> NSManagedObjectContext {
        if shouldFillDatabase() {
            let context = setUpMainContext()
            fillDatabase(withContext: context)
            return context
            
        }
        if !NSUserDefaults.didMovePrepopulatedDatabase { moveDatabase() }
        return setUpMainContext()
    }
    
    private static func moveDatabase() {
        try! NSFileManager.defaultManager().copyItemAtURL(prepopulatedStoreURL!, toURL: targetStoreURL)
        NSUserDefaults.didMovePrepopulatedDatabase = true
    }
}

private extension PrepopulatedCoreDataStack {
    
    private static func shouldFillDatabase() -> Bool {        
        guard let path = targetBundle.pathForResource(Keys.DatabaseFileName, ofType: Keys.DatabaseFileExtenstion) else { return true }
        return !NSFileManager.defaultManager().fileExistsAtPath(path)
    }
    
    private static func fillDatabase(withContext context: NSManagedObjectContext) {
        guard
            let path = targetBundle.pathForResource(Keys.JSONFileName, ofType: Keys.JSONFileExtenstion),
            let cityData = NSData(contentsOfFile: path) else {
                assertionFailure("Cannot open JSON file from main bundle. Should not never happen. Get in da chopper")
                return
        }
        do {
            let context = setUpMainContext()
            if let cities = try NSJSONSerialization.JSONObjectWithData(cityData, options: .MutableContainers) as? [[String: AnyObject]] {
                cities.forEach({ (jsonRecord) in
                    guard City.deserialize(jsonRecord, andInsertInContext: context) != nil else {
                        assertionFailure("Something went wrong when tried to deserialize record: \(jsonRecord)")
                        return
                    }
                })
            }
            context.saveOrRollback()
        }
        catch {
            assertionFailure("Cannot parse JSON file with cities data, because of the error: \(error). Should not never happen. Abort the ship")
        }
    }
}

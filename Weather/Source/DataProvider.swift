//
//  AppDelegate.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit


protocol DataProvider: class {
    associatedtype Object
    func objectAtIndexPath(indexPath: NSIndexPath) -> Object
    func numberOfItemsInSection(section: Int) -> Int
}

protocol DataProviderDelegate: class {
    associatedtype Object
    func dataProviderDidUpdate(updates: [DataProviderUpdate<Object>]?)
    func dataProviderShouldRequestDataUpdate() -> Bool
}

extension DataProviderDelegate {
    func dataProviderShouldRequestDataUpdate() -> Bool {
        return true
    }
}

enum DataProviderUpdate<Object> {
    case Insert(NSIndexPath)
    case Update(NSIndexPath, Object)
    case Move(NSIndexPath, NSIndexPath)
    case Delete(NSIndexPath)
}


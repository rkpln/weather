//
//  AppDelegate.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

protocol DataSourceDelegate: class {
    associatedtype Object
    associatedtype Environment
    func cellIdentifierForObject(object: Object) -> String
    func environmentForObject(object: Object) -> Environment
}


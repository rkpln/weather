//
//  DownloadOperation.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/30/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit
import CoreData


protocol ConfigurableOperationType: class {
    func configure(withSession session: NSURLSession)
}

class DownloadOperation: ConcurrentOperation, ManagedObjectContextSettable, ConfigurableOperationType {
    
    let incomingData = NSMutableData()
    var managedObjectContext: NSManagedObjectContext!
    var request: NSURLRequest?
    var sessionTask: NSURLSessionTask?
    
    init(withRequest request: NSURLRequest!) {
        self.request = request
    }
    
    func configure(withSession session: NSURLSession) {
        guard let request = request else { fatalError("Request should be already set") }
        sessionTask = session.dataTaskWithRequest(request)
    }
    
    func completeOperation() -> (Bool, NSError?) { return (true, nil) }
    
    override func start() {
        sessionTask?.resume()
    }
    
    
    // MARK: - Private
    private var innerContext: NSManagedObjectContext?
    private var error: NSError?
}


extension DownloadOperation {

    func handle(data data: NSData) {
        guard cancelled == false else {
            state = .Finished
            sessionTask?.cancel()
            return
        }
        
        incomingData.appendData(data)
    }
    
    func handle(response response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        guard
            cancelled == false,
            let httpRequest = response as? NSHTTPURLResponse,
            let status = httpRequest.status where status == .Success
        else {
            state = .Finished
            sessionTask?.cancel()
            return
        }
        completionHandler(.Allow)
    }
    
    func handle(completeWithError error: NSError?) {
        defer { state = .Finished }
        guard cancelled == false else {
            sessionTask?.cancel()
            return
        }
        if let unwrappedError = error {
            self.error = unwrappedError
            return
        }
        if let processingError = completeOperation().1 {
            self.error = processingError
        }
    }
}

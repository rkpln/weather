//
//  FetchConditionsNetworkOperation.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/30/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit


private enum  FetchConditionsOperationError {
    case NoData
    case InvalidJSON
    
    var description: String {
        switch self {
            case .NoData: return "Received empty data from server."
            case .InvalidJSON: return "Receieved invalid JSON from server."
        }
    }
    
    var statusCode: Int {
        switch self {
            case .NoData: return 2001
            case .InvalidJSON: return 2002
        }
    }
}


final class FetchConditionsNetworkOperation: DownloadOperation {
    
    override func completeOperation() -> (Bool, NSError?) {
        guard incomingData.length > 0 else {
            return (false, NSError.networkControllerError(FetchConditionsOperationError.NoData.statusCode, localizedDescription: FetchConditionsOperationError.NoData.description))
        }
        
        do {
            guard let data: [String : AnyObject] = try NSJSONSerialization.JSONObjectWithData(incomingData, options: .MutableContainers) as?  [String : AnyObject] else {
                return (false, NSError.networkControllerError(FetchConditionsOperationError.InvalidJSON.statusCode, localizedDescription: FetchConditionsOperationError.InvalidJSON.description))
            }
            print("\(data.description)")
            return (true, nil)
        }
        catch {
            let wrappedError = error as NSError
            return (false, NSError.networkControllerError(wrappedError.code, localizedDescription: wrappedError.localizedDescription))
        }
    }

}

//
//  AppDelegate.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import CoreData


class FetchedResultsDataProvider<Delegate: DataProviderDelegate, SyncOperationBuilder: SyncOperationBuilderType, OperationLauncher: NetworkOperationsLauncher where Delegate.Object == SyncOperationBuilder.SyncableObject, SyncOperationBuilder.BuildedOperation == OperationLauncher.LaunchableOperation>: NSObject, NSFetchedResultsControllerDelegate, DataProvider {

    typealias Object = Delegate.Object

    init(fetchedResultsController: NSFetchedResultsController, delegate: Delegate, syncOperationBuilder: SyncOperationBuilder) {
        self.fetchedResultsController = fetchedResultsController
        self.delegate = delegate
        self.syncOperationBuilder = syncOperationBuilder
        super.init()
        fetchedResultsController.delegate = self
        if delegate.dataProviderShouldRequestDataUpdate() {
            try! fetchedResultsController.performFetch()
        }
    }

    func reconfigureFetchRequest(@noescape block: NSFetchRequest -> ()) {
        NSFetchedResultsController.deleteCacheWithName(fetchedResultsController.cacheName)
        block(fetchedResultsController.fetchRequest)
        guard delegate.dataProviderShouldRequestDataUpdate() else { return }
        do { try fetchedResultsController.performFetch() } catch { fatalError("fetch request failed") }
        delegate.dataProviderDidUpdate(nil)
    }

    func objectAtIndexPath(indexPath: NSIndexPath) -> Object {
        guard let result = fetchedResultsController.objectAtIndexPath(indexPath) as? Object else { fatalError("Unexpected object at \(indexPath)") }
        return result
    }

    func numberOfItemsInSection(section: Int) -> Int {
        guard let sec = fetchedResultsController.sections?[section] else { return 0 }
        return sec.numberOfObjects
    }


    // MARK: Private
    private let fetchedResultsController: NSFetchedResultsController
    private let syncOperationBuilder: SyncOperationBuilder!
    private weak var delegate: Delegate!
    private var updates: [DataProviderUpdate<Object>] = []


    // MARK: NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        updates = []
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
            updates.append(.Insert(indexPath))
        case .Update:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            let object = objectAtIndexPath(indexPath)
            updates.append(.Update(indexPath, object))
        case .Move:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
            updates.append(.Move(indexPath, newIndexPath))
        case .Delete:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            updates.append(.Delete(indexPath))
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        delegate.dataProviderDidUpdate(updates)
    }
    
}

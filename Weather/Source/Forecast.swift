//
//  Forecast.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/14/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData


// MARK: – Forecast
public final class Forecast: NSManagedObject {

    @NSManaged var date: NSDate!
    @NSManaged var humidity: NSNumber!
    @NSManaged var pressure: NSNumber!
    @NSManaged var condition: NSNumber!
    @NSManaged var forecastFor: WeatherReport!
    
    
    var temperatureUnit: KelvinTemperature {
        return KelvinTemperature(temperature)
    }
    
    // MARK: – Private
    private struct Keys {
        static let Date = "date"
    }
    
    @NSManaged private var temperature: NSNumber!
}


// MARK: – ManagedObjectType
extension Forecast: ManagedObjectType {
    
    public static var entityName: String {
        return "Forecast"
    }
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: Keys.Date, ascending: false)]
    }
}




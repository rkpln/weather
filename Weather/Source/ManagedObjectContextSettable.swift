//
//  ManagedObjectContextSettable.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/17/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import CoreData

protocol ManagedObjectContextSettable: class {
    var managedObjectContext: NSManagedObjectContext! { get set }
}

protocol NetworkControllerSettable: class {
    var networkControler: NetworkOperationsController! { get set }
}
//
//  NetworkController.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/26/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData


// MARK: – NetworkOperationsController
class NetworkOperationsController: NSObject, ManagedObjectContextSettable {
    
    var managedObjectContext: NSManagedObjectContext!
    
    override init() {
        operationQueue = NSOperationQueue()
        super.init()
        setUpOperationQueue()
        setUpSession()
    }
    
    // MARK: – Private
    private let operationQueue: NSOperationQueue!
    private var session: NSURLSession!
    
    private func setUpOperationQueue() {
        operationQueue.qualityOfService = .Background
        operationQueue.maxConcurrentOperationCount = 2
    }
    
    private func setUpSession() {
        session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: operationQueue)
    }
}

extension NetworkOperationsController: NetworkOperationsLauncher {
    func enqueueOperation(operation: DownloadOperation) {
        operation.managedObjectContext = managedObjectContext
        operationQueue.addOperation(operation)
    }
}

extension NSOperationQueue {
    func findOperation(byTask task: NSURLSessionTask) -> DownloadOperation? {
        return self.operations.findElement({ (operation) -> Bool in
            guard let downloadOperation = operation as? DownloadOperation else { return false }
            return downloadOperation.sessionTask == task
        }) as? DownloadOperation
    }
}

// MARK: – NSURLSessionDelegate
extension NetworkOperationsController: NSURLSessionDelegate {
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if let operation = operationQueue.findOperation(byTask: task) {
            operation.handle(completeWithError: error)
        }
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        if let operation = operationQueue.findOperation(byTask: dataTask) {
            operation.handle(data: data)
        }
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        if let operation = operationQueue.findOperation(byTask: dataTask) {
            operation.handle(response: response, completionHandler: completionHandler)
        }
    }
}

extension NSError {
    static func networkControllerError(code: Int, localizedDescription: String) -> NSError {
        return NSError(domain: "com.weather.network.controller", code: code, userInfo: [
                NSLocalizedDescriptionKey : localizedDescription
        ])
    }
}

extension NSHTTPURLResponse {
    
    enum Status: Int {
        case Success = 200
        case InvalidAPIkey = 401
    }
    
    var status: Status? {
        return Status(rawValue: statusCode)
    }
}

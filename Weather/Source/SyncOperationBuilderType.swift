//
//  SyncOperationBuilderType.swift
//  Weather
//
//  Created by Roman Kopaliani on 9/1/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation

protocol SyncOperationBuilderType {
    associatedtype SyncableObject
    associatedtype BuildedOperation
    func buildSyncOperation() -> BuildedOperation?
}
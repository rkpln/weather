//
//  Utilities.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/12/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation


// MARK: – String Ext: Normalized For Search
extension String {
    
    public var normalizedForSearch: String {
        let transformed = stringByApplyingTransform("Any-Latin; Latin-ASCII; Lower", reverse: false)
        return transformed ?? ""
    }
}


// MARK: – NSURL Ext: Helper URLs
extension NSURL {
    static func cachesURL() -> NSURL {
        return try! NSFileManager.defaultManager().URLForDirectory(NSSearchPathDirectory.CachesDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true).URLByAppendingPathComponent(NSUUID().UUIDString)
    }
    
    static var documentsDirectoryURL: NSURL {
        return try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
    }
}


// MARK: – SequenceType Ext: Find Element
extension SequenceType {
    func findElement (match: Generator.Element->Bool) -> Generator.Element? {
        for element in self where match(element) {
            return element
        }
        return nil
    }
}


// MARK: – NSDate Ext: Comparable
extension NSDate: Comparable {}

public func ==(l: NSDate, r: NSDate) -> Bool {
    return l.timeIntervalSince1970 == r.timeIntervalSince1970
}

public func <(l: NSDate, r: NSDate) -> Bool {
    return l.timeIntervalSince1970 < r.timeIntervalSince1970
}

public func >(l: NSDate, r: NSDate) -> Bool {
    return l.timeIntervalSince1970 > r.timeIntervalSince1970
}

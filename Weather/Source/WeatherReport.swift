//
//  WeatherReport.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/25/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation
import CoreData


public final class WeatherReport: NSManagedObject {

    @NSManaged var requestedAt: NSDate!
    @NSManaged var reportOf: City!

    static func insert(inMOC context: NSManagedObjectContext, city: City) -> WeatherReport {
        let report: WeatherReport = context.insertObject()
        report.reportOf = city
        return report
    }

    override public func awakeFromInsert() {
        super.awakeFromInsert()
        requestedAt = NSDate()
    }

    var sortedConditions: [Forecast]? {
        return forecasts?.sort {
            return $0.date < $1.date
        }
    }

    // MARK: –  Private
    private struct Keys {
        static let DateRequestedAt = "requestedAt"
    }
    @NSManaged private(set) var forecasts: Set<Forecast>?
}


// MARK: – ManagedObjectType
extension WeatherReport: ManagedObjectType {
    
    public static var entityName: String {
        return "WeatherReport"
    }
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: Keys.DateRequestedAt, ascending: true)]
    }
}

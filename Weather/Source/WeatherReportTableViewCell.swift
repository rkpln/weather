//
//  WeatherReportTableViewCell.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/17/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit


final class WeatherReportTableViewCell: UITableViewCell {

    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var weatherImageView: UIImageView!
}


// MARK: – ConfigurableCell
extension WeatherReportTableViewCell: ConfigurableCell {
    
    func configureForObject(report: WeatherReport, inEnvironment unit: TemperatureUnit) {
        cityLabel.text = report.reportOf.name
        timeLabel.text = sharedDateFormatter.stringFromDate(report.requestedAt)
        guard
            let conditions = report.sortedConditions where conditions.isEmpty == false,
            let lastCondition = conditions.last else {
                temperatureLabel.text = "N/A"
            return
        }

        let temperature = lastCondition.temperatureUnit
        temperatureLabel.text = "\(temperature.value(inUnit: unit))" + unit.displayString
    }
}


// MARK: – NSDateFormatter
private let sharedDateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateStyle = .MediumStyle
    formatter.timeStyle = .ShortStyle
    formatter.doesRelativeDateFormatting = true
    formatter.formattingContext = .Standalone
    return formatter
}()

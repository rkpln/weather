//
//  WeatherReportsSyncOperationBuilder.swift
//  Weather
//
//  Created by Roman Kopaliani on 9/1/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import Foundation

final class WeatherReportsSyncOperationBuilder: SyncOperationBuilderType {
    
    typealias SyncableObject = WeatherReport
    var reports: [WeatherReport]!
    
    init() {
        reports = []
    }
    
    func buildSyncOperation() -> DownloadOperation? {
        guard
            reports.isEmpty == false,
            let url = formatURL(forWeatherReports: reports)
            else { return nil }
        return FetchConditionsNetworkOperation(withRequest: NSURLRequest(URL: url))
    }
    
    private func formatURL(forWeatherReports reports:[WeatherReport]) -> NSURL? {
        let urlComponents = NSURLComponents()
        var finalQueryItems = queryItems(forWeatherReports: reports)
        urlComponents.scheme = "http"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/" + lastPathComponent(forWeatherReports: reports)
        let apiKey = NSURLQueryItem(name: "APPID", value: "1a77fd2ac0fd66211264f0bf6c9f355f")
        finalQueryItems.append(apiKey)
        urlComponents.queryItems = finalQueryItems
        return urlComponents.URL
    }
    
    private func queryItems(forWeatherReports reports:[WeatherReport]) -> [NSURLQueryItem] {
        let ids = reports.map { "\($0.reportOf.id)"}.joinWithSeparator(",")
        return [NSURLQueryItem(name: "id", value: ids)]
    }
    
    private func lastPathComponent(forWeatherReports reports:[WeatherReport]) -> String {
        return reports.count > 1 ? "weather" : "group"
    }
}

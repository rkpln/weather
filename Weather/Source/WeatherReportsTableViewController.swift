//
//  CitiesTableViewController.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/17/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import UIKit
import CoreData

final class WeatherReportsTableViewController: UITableViewController, ManagedObjectContextSettable, NetworkControllerSettable {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var unitSwitcher: UISegmentedControl! {
        didSet {
            unitSwitcher.fillSegments()
        }
    }
    
    var networkControler: NetworkOperationsController!
    var managedObjectContext: NSManagedObjectContext!
    
    @IBAction func didSwitch(segmentedControl: UISegmentedControl) {
        dataSource.processUpdates(nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
        
    // MARK: – Private
    private var temperatureUnit: TemperatureUnit = .Celsius
    private typealias WeatherReportsDataProvider = FetchedResultsDataProvider<WeatherReportsTableViewController, WeatherReportsSyncOperationBuilder, NetworkOperationsController>
    private var dataProvider: WeatherReportsDataProvider!
    private var dataSource: TableViewDataSource<WeatherReportsTableViewController, WeatherReportsDataProvider, WeatherReportTableViewCell>!
    
    private func setUpTableView() {
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        setUpDataSource()
    }
    
    private func setUpDataSource() {
        let fetchRequest = WeatherReport.allEntitiesFetchRequest { request in
            request.sortDescriptors = WeatherReport.defaultSortDescriptors
            request.relationshipKeyPathsForPrefetching = ["reportOf"]
        };
        let fetchRequestController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: "com.weather.weatherreportstableviewcontroller.cache")
        let operationBuilder = WeatherReportsSyncOperationBuilder()
        dataProvider = FetchedResultsDataProvider(fetchedResultsController: fetchRequestController, delegate: self, syncOperationBuilder: operationBuilder)
        dataSource = TableViewDataSource(tableView: tableView, dataProvider: dataProvider, delegate: self)
    }
}


// MARK: – DataSourceDelegate
extension WeatherReportsTableViewController: DataSourceDelegate {
    
    func cellIdentifierForObject(object: WeatherReport) -> String {
        return "WeatherReportCell"
    }
    
    func environmentForObject(object: WeatherReport) -> TemperatureUnit {
        return temperatureUnit
    }
}


// MARK: – DataProviderDelegate
extension WeatherReportsTableViewController: DataProviderDelegate {
    
    func dataProviderDidUpdate(updates: [DataProviderUpdate<WeatherReport>]?) {
        dataSource.processUpdates(updates)
    }
}


// MARK: – SegueHandlerType
extension WeatherReportsTableViewController: SegueHandlerType {
    
    enum SegueIdentifier: String {
        case ShowCitySearch = "showSearch"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segueIdentifierForSegue(segue) {
            case .ShowCitySearch:
                guard let nc = segue.destinationViewController as? UINavigationController,
                    let vc = nc.viewControllers.first as? ManagedObjectContextSettable else { fatalError("Destination Controller doesn't conform to ManagedObjectContextSettable")}
            vc.managedObjectContext = managedObjectContext
        }
    }
    
    @IBAction func closeSearch(unwindSegue: UIStoryboardSegue) {}
}

// MARK: – UISegmentedControl 
private extension UISegmentedControl {
    
    func fillSegments() {
        for idx in 0..<numberOfSegments {
            guard let unit = TemperatureUnit(rawValue: idx) else { fatalError("Invalid unit index when filling data") }
            setTitle(unit.displayString, forSegmentAtIndex: idx)
        }
    }
    
    var unit: TemperatureUnit {
        guard let unit = TemperatureUnit(rawValue: selectedSegmentIndex) else { fatalError("Invalid unit index") }
        return unit
    }
}
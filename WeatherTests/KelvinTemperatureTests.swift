//
//  KelvinTemperatureTests.swift
//  Weather
//
//  Created by Roman Kopaliani on 8/23/16.
//  Copyright © 2016 Roman Kopaliani. All rights reserved.
//

import XCTest
@testable import Weather

class KelvinTemperatureTests: XCTestCase {
    
    var sut: KelvinTemperature!
    var value: Double!
    
    override func setUp() {
        super.setUp()
        
        value = 100
        sut = KelvinTemperature(value)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testKelvinConversion() {
        
        let kelvin100 = KelvinTemperature(value)
        XCTAssertEqual(sut, kelvin100)
    }
    
    func testKelvinCelciusConversion() {
        XCTAssertEqual(sut.value(inUnit: .Celsius), value - 273.15)
    }
    
    func testKelvinFarenheitConversion() {
        XCTAssertEqualWithAccuracy(sut.value(inUnit: .Fahrenheit), -279.67, accuracy: 0.2)
    }
    
}
